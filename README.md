# contentful_ui_extensions

Custom or customized UI extensions used in Qarson Contentful CMS.

## using the UI extensions in Contentful

* [general documentation about UI extensions](https://www.contentful.com/developers/docs/concepts/uiextensions/)
* [SDK](https://github.com/contentful/ui-extensions-sdk/blob/master/docs/ui-extensions-sdk-frontend.md)
* [a few example extensions](https://github.com/contentful/extensions/tree/master/samples) (look into individual HTML files to see how they interact with Contentful) 

### Installing the plugin manually from the GUI

* Deploy it
* Ensure this is visible under https://contentful-ui-ext.dacsoftware.tech/
* Go to a specific space
* Go to Settings > Extensions
* Choose "self-hosted" and provide path to HTML file of specific extension https://contentful-ui-ext.dacsoftware.tech/tinymce/tiny.html
* Assign to specific fields types (e.g. "Text" for editors)

## UI extensions in this repo

### TinyMCE

* TinyMCE editor with basic configuration (can be adapted in tiny.html file)
* Two-directional integration with the specific fields via Contentful SDK
	* Updates content of the editor when the field changes (e.g. triggered by other user updating the same field)
	* Pushes editor changes to the field (so that auto-save works)
* Custom button added to insert images from Contentful assets
	* It will use Asset's description as alt (if set)
	* Unfortunately no possibility to trigger the upload dialog at this time (in they backlog but not planned https://github.com/contentful/ui-extensions-sdk/issues/132)